import mongoose from 'mongoose';

const ValidatorSchema = new mongoose.Schema({
  operator_address: String,
  consensus_pubkey: Object,
  consensus_pubkey: Boolean,
  status: String,
  tokens: String,
  delegator_shares: String,
  description: Object,
  unbonding_height: String,
  unbonding_time: String,
  commission: Object,
  min_self_delegation: String,
});

ValidatorSchema.index({ operator_address: 1 }, { unique: true });

const ValidatorModel = mongoose.model('Validator', ValidatorSchema);

export default ValidatorModel;

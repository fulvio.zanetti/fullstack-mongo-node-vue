'use strict';

require('dotenv').config({ path: `.env.${process.env.NODE_ENV}` });

const SETTINGS = require('./constants/settings');

const express = require('express');

const app = express();

app.listen(SETTINGS.PORT, SETTINGS.HOST, () => {
  console.log(`Running on http://${SETTINGS.HOST}:${SETTINGS.PORT}`);
});
